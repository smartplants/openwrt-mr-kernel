self_path:
{
  preBuild = ''
  '';
  preConfigure = "";
  postBuild = ''
    mkdir tmp_rootfs
    cd tmp_rootfs
    tar xf ../bin/targets/ar71xx/mikrotik/openwrt-ar71xx-mikrotik-device-rb-nor-flash-16m-rootfs.tar.gz
    rm kernel
    rm -rf lib/modules/*
    cp -r ${self_path}/* ./
    rm ../bin/targets/ar71xx/mikrotik/openwrt-ar71xx-mikrotik-device-rb-nor-flash-16m-rootfs.tar.gz
    tar czf ../bin/targets/ar71xx/mikrotik/openwrt-ar71xx-mikrotik-device-rb-nor-flash-16m-rootfs.tar.gz *
    cd ..
    rm -rf tmp_rootfs
  '';
}